package com.codingtask.occupancy;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.codingtask.occupancy.model.RoomOccupancy;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class OccupancyApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	
	@Test
	public <T> void serviceRestTest3_3() {	
		ResponseEntity<List<RoomOccupancy>> rateResponse =
		        restTemplate.exchange("/api/occupancy?premium=3&economy=3",
					                    HttpMethod.GET, 
					                    new HttpEntity<T>(createHeaders("admin", "admin")), 
					                    new ParameterizedTypeReference<List<RoomOccupancy>>() {});
		
		List<RoomOccupancy> roomOccupancy = rateResponse.getBody();
		
		assert(roomOccupancy.get(0).getBookedRooms() == 3
				&& roomOccupancy.get(0).getTotalCost().equals(new BigDecimal(738))
				&& roomOccupancy.get(1).getBookedRooms() == 3
				&& roomOccupancy.get(1).getTotalCost().equals(new BigDecimal(167)));
	}
	
	@Test
	public <T> void serviceRestTest7_5() {		
		ResponseEntity<List<RoomOccupancy>> rateResponse =
		        restTemplate.exchange("/api/occupancy?premium=7&economy=5",
						        		HttpMethod.GET, 
					                    new HttpEntity<T>(createHeaders("admin", "admin")), 
					                    new ParameterizedTypeReference<List<RoomOccupancy>>() {});
		
		List<RoomOccupancy> roomOccupancy = rateResponse.getBody();
		
		assert(roomOccupancy.get(0).getBookedRooms() == 6
				&& roomOccupancy.get(0).getTotalCost().equals(new BigDecimal(1054))
				&& roomOccupancy.get(1).getBookedRooms() == 4
				&& roomOccupancy.get(1).getTotalCost().equals(new BigDecimal(189)));
	}
	
	@Test
	public <T> void serviceRestTest2_7() {		
		ResponseEntity<List<RoomOccupancy>> rateResponse =
		        restTemplate.exchange("/api/occupancy?premium=2&economy=7",
						        		HttpMethod.GET, 
					                    new HttpEntity<T>(createHeaders("admin", "admin")), 
					                    new ParameterizedTypeReference<List<RoomOccupancy>>() {});
		
		List<RoomOccupancy> roomOccupancy = rateResponse.getBody();
		
		assert(roomOccupancy.get(0).getBookedRooms() == 2
				&& roomOccupancy.get(0).getTotalCost().equals(new BigDecimal(583))
				&& roomOccupancy.get(1).getBookedRooms() == 4
				&& roomOccupancy.get(1).getTotalCost().equals(new BigDecimal(189)));
	}
	
	private HttpHeaders createHeaders(String username, String password){
	   return new HttpHeaders() {{
	         String auth = username + ":" + password;
	         byte[] encodedAuth = Base64.encodeBase64( 
	            auth.getBytes(Charset.forName("US-ASCII")) );
	         String authHeader = "Basic " + new String( encodedAuth );
	         set( "Authorization", authHeader );
	      }};
	}

}

