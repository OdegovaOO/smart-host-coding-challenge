package com.codingtask.occupancy;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.codingtask.occupancy.enums.RoomType;
import com.codingtask.occupancy.model.Customer;
import com.codingtask.occupancy.model.RoomOccupancy;
import com.codingtask.occupancy.service.CustomerService;
import com.codingtask.occupancy.service.RoomService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomServiceTests {

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private RoomService roomService;
	
	@Test
	public void upgradeEconomyCustomersTest() {
		int min = 2; 
		List<Customer> economyCustomers = customerService.getEconomyCustomers();
		int initialEconomyCustomersSize = economyCustomers.size(); 
		RoomOccupancy premiumRoomOccupancy = new RoomOccupancy(RoomType.PREMIUM);
		economyCustomers = roomService.upgradeEconomyCustomers(min, economyCustomers, premiumRoomOccupancy);
		
		assert(economyCustomers.size() == (initialEconomyCustomersSize - min)  
				&& premiumRoomOccupancy.getGuests().size() == min);
	}
	
	@Test
	public void fillPremiumRoomsTest() {
		int premiumRoomsCapacity = 2;
		List<Customer> premiumCustomers = customerService.getPremiumCustomers();
		RoomOccupancy premiumRoomOccupancy = new RoomOccupancy(RoomType.PREMIUM);
		roomService.fillPremiumRooms(premiumRoomsCapacity, premiumCustomers, premiumRoomOccupancy);
		
		assert(premiumRoomOccupancy.getGuests().size() == premiumRoomsCapacity);
	}
	
	@Test
	public void fillEconomyRoomsTest() {
		int economyRoomsCapacity = 2;
		List<Customer> economyCustomers = customerService.getEconomyCustomers();
		RoomOccupancy economyRoomOccupancy = new RoomOccupancy(RoomType.ECONOMY);
		roomService.fillEconomyRooms(economyRoomsCapacity, economyCustomers, economyRoomOccupancy);
		
		assert(economyRoomOccupancy.getGuests().size() == economyRoomsCapacity);
	}
	
	@Test
	public void updateTotalCostTest() {
		List<Customer> economyCustomers = customerService.getEconomyCustomers();
		RoomOccupancy economyRoomOccupancy = new RoomOccupancy(RoomType.ECONOMY);
		economyRoomOccupancy.addGuests(economyCustomers);
		roomService.updateTotalCost(economyRoomOccupancy);
		
		assert(economyRoomOccupancy.getTotalCost().equals(new BigDecimal(189)));
	}
	
	@Test
	public void updateBookedRoomsTest() {
		List<Customer> economyCustomers = customerService.getEconomyCustomers();
		RoomOccupancy economyRoomOccupancy = new RoomOccupancy(RoomType.ECONOMY);
		economyRoomOccupancy.addGuests(economyCustomers);
		roomService.updateBookedRooms(economyRoomOccupancy);
		
		assert(economyRoomOccupancy.getBookedRooms() == 4);
	}
	
}
