package com.codingtask.occupancy;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.codingtask.occupancy.model.Customer;
import com.codingtask.occupancy.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTests {

	@Autowired
	private CustomerService customerService;
	
	@Test
	public void getCustomersTest() {
		List<Customer> customers = customerService.getCustomers();
		assert(customers.size() == 10);
	}
	
	@Test
	public void getEconomyCustomersTest() {
		List<Customer> customers = customerService.getEconomyCustomers();
		assert(customers.size() == 4);
	}
	
	@Test
	public void getPremiumCustomersTest() {
		List<Customer> customers = customerService.getPremiumCustomers();
		assert(customers.size() == 6);
	}
	
}
