package com.codingtask.occupancy;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.codingtask.occupancy.model.RoomOccupancy;
import com.codingtask.occupancy.service.OccupancyService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OccupancyServiceTests {
	
	@Autowired
	private OccupancyService occupancyService;

	@Test
	public void serviceTest3_3() {
		List<RoomOccupancy> roomOccupancy = occupancyService.getOccupancy(3, 3);
		assert(roomOccupancy.get(0).getBookedRooms() == 3
				&& roomOccupancy.get(0).getTotalCost().equals(new BigDecimal(738))
				&& roomOccupancy.get(1).getBookedRooms() == 3
				&& roomOccupancy.get(1).getTotalCost().equals(new BigDecimal(167)));
	}
	
	@Test
	public void serviceTest7_5() {
		List<RoomOccupancy> roomOccupancy = occupancyService.getOccupancy(7, 5);
		assert(roomOccupancy.get(0).getBookedRooms() == 6
				&& roomOccupancy.get(0).getTotalCost().equals(new BigDecimal(1054))
				&& roomOccupancy.get(1).getBookedRooms() == 4
				&& roomOccupancy.get(1).getTotalCost().equals(new BigDecimal(189)));
	}
	
	@Test
	public void serviceTest2_7() {
		List<RoomOccupancy> roomOccupancy = occupancyService.getOccupancy(2, 7);
		assert(roomOccupancy.get(0).getBookedRooms() == 2
				&& roomOccupancy.get(0).getTotalCost().equals(new BigDecimal(583))
				&& roomOccupancy.get(1).getBookedRooms() == 4
				&& roomOccupancy.get(1).getTotalCost().equals(new BigDecimal(189)));
	}
	
}
