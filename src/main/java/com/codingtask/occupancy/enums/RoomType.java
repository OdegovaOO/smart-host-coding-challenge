package com.codingtask.occupancy.enums;

public enum RoomType {
	PREMIUM,
	ECONOMY;
}
