package com.codingtask.occupancy.service;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.codingtask.occupancy.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Override
	public List<Customer> getCustomers() {
		List<Customer> customers = new ArrayList<>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			BigDecimal[] customerValues = mapper.readValue(new File("data/smarthost_hotel_guests.json"), BigDecimal[].class);
			customers = Arrays.asList(customerValues).stream()
					.map(value -> new Customer(value))
					.collect(Collectors.toList());
			return customers;
		} catch (Exception e) {
			e.printStackTrace();
			return customers;
		}
	}

	@Override
	public List<Customer> getEconomyCustomers() {
		return getCustomers().stream()
				.filter(customer -> customer.isEconomy())
				.sorted((Customer o1, Customer o2) -> o2.getPayingCapacity().compareTo(o1.getPayingCapacity()))
				.collect(Collectors.toList());
	}

	@Override
	public List<Customer> getPremiumCustomers() {
		return getCustomers().stream()
				.filter(customer -> customer.isPremium())
				.sorted((Customer o1, Customer o2) -> o2.getPayingCapacity().compareTo(o1.getPayingCapacity()))
				.collect(Collectors.toList());
	}

}
