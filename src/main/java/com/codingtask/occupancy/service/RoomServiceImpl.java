package com.codingtask.occupancy.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import com.codingtask.occupancy.model.Customer;
import com.codingtask.occupancy.model.RoomOccupancy;

@Service
public class RoomServiceImpl implements RoomService{

	@Override
	public List<Customer> upgradeEconomyCustomers(int min, List<Customer> economyCustomers,
			RoomOccupancy premiumRoomOccupancy) {
		for(int i = 0; i < min; i++) {
			premiumRoomOccupancy.addGuest(economyCustomers.get(i));
			economyCustomers.remove(i);
		}
		return economyCustomers;
	}

	@Override
	public void fillPremiumRooms(int premiumRoomsCapacity, List<Customer> premiumCustomers,
			RoomOccupancy premiumRoomOccupancy) {
		int bookedRooms = Math.min(premiumRoomsCapacity, premiumCustomers.size());
		for(int i = 0; i < bookedRooms; i++) {
			premiumRoomOccupancy.addGuest(premiumCustomers.get(i));
		}
		
	}

	@Override
	public void fillEconomyRooms(int economyRoomsCapacity, List<Customer> economyCustomers,
			RoomOccupancy economyRoomOccupancy) {
		int bookedRooms = Math.min(economyRoomsCapacity, economyCustomers.size());
		for(int i = 0; i < bookedRooms; i++) {
			economyRoomOccupancy.addGuest(economyCustomers.get(i));
		}
	}
	
	@Override
	public void updateTotalCost(RoomOccupancy roomOccupancy) {
		if (roomOccupancy.getGuests() == null || roomOccupancy.getGuests().isEmpty()) {
			roomOccupancy.setTotalCost(new BigDecimal(0));
		} else {
			double sum = roomOccupancy.getGuests().stream().mapToDouble(customer -> customer.getPayingCapacity().doubleValue()).sum();
			roomOccupancy.setTotalCost(new BigDecimal(sum)); 
		}	
		
	}

	@Override
	public void updateBookedRooms(RoomOccupancy roomOccupancy) {
		if (roomOccupancy.getGuests() == null || roomOccupancy.getGuests().isEmpty()) {
			roomOccupancy.setBookedRooms(0);
		} else {
			roomOccupancy.setBookedRooms(roomOccupancy.getGuests().size()); 
		}	
	}

}
