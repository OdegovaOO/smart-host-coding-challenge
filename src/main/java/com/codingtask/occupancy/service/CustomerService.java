package com.codingtask.occupancy.service;

import java.util.List;

import com.codingtask.occupancy.model.Customer;

public interface CustomerService {
	
	public List<Customer> getCustomers();

	public List<Customer> getEconomyCustomers();

	public List<Customer> getPremiumCustomers();

}
