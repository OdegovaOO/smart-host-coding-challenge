package com.codingtask.occupancy.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codingtask.occupancy.enums.RoomType;
import com.codingtask.occupancy.model.Customer;
import com.codingtask.occupancy.model.RoomOccupancy;

@Service
public class OccupancyServiceImpl implements OccupancyService {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private RoomService roomService;

	@Override
	public List<RoomOccupancy> getOccupancy(int premiumRoomsCapacity, int economyRoomsCapacity) {
		List<Customer> premiumCustomers = customerService.getPremiumCustomers();
		List<Customer> economyCustomers = customerService.getEconomyCustomers();
		
		RoomOccupancy premiumRoomOccupancy = new RoomOccupancy(RoomType.PREMIUM);
		roomService.fillPremiumRooms(premiumRoomsCapacity, premiumCustomers, premiumRoomOccupancy);

		if (premiumCustomers.size() < premiumRoomsCapacity && economyCustomers.size() > economyRoomsCapacity) {
			int freePremiumRooms = premiumRoomsCapacity - premiumCustomers.size();
			int excessEconomyGuests = economyCustomers.size() - economyRoomsCapacity;
			int upgradeEconomyCustomersNumber = Math.min(freePremiumRooms, excessEconomyGuests);
			
			economyCustomers = roomService.upgradeEconomyCustomers(upgradeEconomyCustomersNumber, economyCustomers, premiumRoomOccupancy);
		}
		
		RoomOccupancy economyRoomOccupancy = new RoomOccupancy(RoomType.ECONOMY);
		roomService.fillEconomyRooms(economyRoomsCapacity, economyCustomers, economyRoomOccupancy);
		
		updateRoomOccupancyData(premiumRoomOccupancy);
		updateRoomOccupancyData(economyRoomOccupancy);
		
		List<RoomOccupancy> theRoomOccupancy = new ArrayList<>();
		theRoomOccupancy.add(premiumRoomOccupancy);
		theRoomOccupancy.add(economyRoomOccupancy);
		
		return theRoomOccupancy;
	}
	
	private void updateRoomOccupancyData(RoomOccupancy roomOccupancy) {
		roomService.updateTotalCost(roomOccupancy);
		roomService.updateBookedRooms(roomOccupancy);
	}
	
}
