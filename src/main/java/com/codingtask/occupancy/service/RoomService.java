package com.codingtask.occupancy.service;

import java.util.List;

import com.codingtask.occupancy.model.Customer;
import com.codingtask.occupancy.model.RoomOccupancy;

public interface RoomService {

	public List<Customer> upgradeEconomyCustomers(int min, List<Customer> economyCustomers, RoomOccupancy premiumRoomOccupancy);

	public void fillPremiumRooms(int premiumRoomsCapacity, List<Customer> premiumCustomers, RoomOccupancy premiumRoomOccupancy);

	public void fillEconomyRooms(int economyRoomsCapacity, List<Customer> economyCustomers, RoomOccupancy economyRoomOccupancy);
	
    public void updateTotalCost(RoomOccupancy roomOccupancy);
	
	public void updateBookedRooms(RoomOccupancy roomOccupancy);
}
