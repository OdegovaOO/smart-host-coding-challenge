package com.codingtask.occupancy.service;

import java.util.List;

import com.codingtask.occupancy.model.RoomOccupancy;

public interface OccupancyService {

	public List<RoomOccupancy> getOccupancy(int premiumRoomsCapacity,  int economyRoomsCapacity);
	
}
