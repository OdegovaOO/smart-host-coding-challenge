package com.codingtask.occupancy.model;

import java.math.BigDecimal;

public class Customer {
	
	final static private BigDecimal threshold = BigDecimal.valueOf(100);
	
	private BigDecimal payingCapacity;
	
	public Customer() {
	}
	
	public Customer(BigDecimal thePayingCapacity) {
		this.payingCapacity = thePayingCapacity;
	}

	public BigDecimal getPayingCapacity() {
		return payingCapacity;
	}

	public void setPayingCapacity(BigDecimal payingCapacity) {
		this.payingCapacity = payingCapacity;
	}

	public boolean isPremium() {
		return payingCapacity == null ? false : payingCapacity.compareTo(threshold) >= 0;
	}
	
	public boolean isEconomy() {
		return payingCapacity == null ? false : payingCapacity.compareTo(threshold) < 0;
	}
	
}
