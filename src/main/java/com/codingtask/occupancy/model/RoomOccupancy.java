package com.codingtask.occupancy.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.codingtask.occupancy.enums.RoomType;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class RoomOccupancy {

	private RoomType roomType;
	private BigDecimal totalCost;
	private int bookedRooms;
	private List<Customer> guests;
	
	
	public RoomOccupancy() {
	}
	
	public RoomOccupancy(RoomType roomType) {
		this.roomType = roomType;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public int getBookedRooms() {
		return bookedRooms;
	}

	public void setBookedRooms(int bookedRooms) {
		this.bookedRooms = bookedRooms;
	}

	@JsonIgnore
	public List<Customer> getGuests() {
		return guests;
	}

	public void setGuests(List<Customer> guests) {
		this.guests = guests;
	}
	
	public void addGuest(Customer theCustomer) {
		if (guests == null) {
			guests = new ArrayList<>();
		}
		guests.add(theCustomer);
	}
	
	public void addGuests(List<Customer> theCustomers) {
		if (guests == null) {
			guests = new ArrayList<>();
		}
		guests.addAll(theCustomers);
	}

	@Override
	public String toString() {
		return "RoomOccupancy [roomType=" + roomType + ", totalCost=" + totalCost + ", bookedRooms=" + bookedRooms
				+ "]";
	}

}
