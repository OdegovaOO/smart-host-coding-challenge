package com.codingtask.occupancy.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codingtask.occupancy.model.RoomOccupancy;
import com.codingtask.occupancy.service.OccupancyService;

@RestController
@RequestMapping("/api")
public class OccupancyRestController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private OccupancyService occupancyService;
	
	
	@GetMapping("/occupancy")
	public List<RoomOccupancy> getOccupancy(@RequestParam("premium") int premiumRoomsCapacity, 
											@RequestParam("economy") int economyRoomsCapacity) {
		
		logger.info("getOccupancy for premiumRoomsCapacity = " + premiumRoomsCapacity + ", economyRoomsCapacity = " + economyRoomsCapacity);
		
		List<RoomOccupancy> roomOccupancy = occupancyService.getOccupancy(premiumRoomsCapacity, economyRoomsCapacity);
		
		logger.info("roomOccupancy: " + roomOccupancy);
		
		return roomOccupancy;
	}
	
}
