# Smart Host Code Challenge

Build a room occupancy optimization tool for one of our hotel clients! 
Our customer has a certain number of free rooms each night, as well as potential guests that would like to book a room for that night.

## Getting Started

### Prerequisites

```
JDK 8
Maven
```

### Installing & Running

```
mvn spring-boot:run
```

## Running the tests

All tests can be run as following

```
mvn test
```

## Sending test requests

```
user = admin
password = admin

Example: http://localhost:8080/api/occupancy?premium=2&economy=7
```

## Considerations / Presumptions

1. BigDecimal preferred over Double for consistent arithmetic operations.

2. Restfull service use spring-security for Authentication

3. In the first test (3;3) total economy cost is 167 EUR